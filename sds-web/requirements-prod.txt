django-widget-tweaks
#widget_tweaks
django-ckeditor
#ckeditor

#Django(LTS)
Django==2.2.5
#strictly separate the settings parameters
python-decouple
#safe db url,credentials
dj-database-url


gunicorn==19.9.0
psycopg2-binary==2.8.3

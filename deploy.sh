ssh -o StrictHostKeyChecking=no $CONNECT_DEPLOY_INSTANCE << 'ENDSSH'
  cd sds-web
  export $(cat .env.prod | xargs)
  docker login -u $CI_DOCKER_DEPLOY_USER -p $CI_DOCKER_DEPLOY_KEY $CI_REGISTRY
  docker pull $IMAGE:web
  docker pull $IMAGE:nginx
  docker-compose -f docker-compose.prod.yml up -d
  docker-compose -f docker-compose.prod.yml exec -T web python manage.py collectstatic --no-input --clear
  docker update --restart unless-stopped $(docker ps -q)
ENDSSH
